import $ from 'jquery';

class Mobilemenu {
	constructor() {
		this.siteHeader = $(".site-header");
		this.menuIcon = $(".site-header__menu-icon");
		this.menuContent = $(".site-header__menu-content");
		this.events();

	}

	events() {
		this.menuIcon.click(this.toggleTheMenu.bind(this)); //binds "this" to toggleTheMenu 
	}														//instead of menuIcon
	

	toggleTheMenu() {
		this.menuContent.toggleClass("site-header__menu-content--is-visible");  //"this" would reference menuIcon
		this.siteHeader.toggleClass('site-header--is-expanded');				//if we didn't bind above
		this.menuIcon.toggleClass('site-header__menu-icon--close-x');
	}																				
}

export default Mobilemenu;