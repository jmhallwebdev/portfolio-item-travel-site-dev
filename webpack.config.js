module.exports = {
	entry: {
		App: './app/assets/scripts/App.js',
		Vendor: './app/assets/scripts/Vendor.js'
	},
	output: {
		path: __dirname + '/app/temp/scripts',
		filename: "[name].js"
	},
	module: {
		loaders: [
		{
			loader: 'babel-loader',
			query: {
				presets: ['es2015']
			},
			test: /\.js$/,	//tells webpack to only load babel for .js files
			exclude: /node_modules/     //tells babel what files to ignore
		}
	  ]
	}
}